government = tribal
add_government_reform = slave_state
government_rank = 1
primary_culture = brown_orc
religion = great_dookan
technology_group = tech_orcish
capital = 740

1421.1.1 = {
	monarch = {
		name = "Ordash"
		dynasty = "Thunderfist"
		birth_date = 1410.10.4
		adm = 3
		dip = 4
		mil = 4
	}
}