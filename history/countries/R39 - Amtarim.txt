government = monarchy
add_government_reform = jamindar_reform
government_rank = 1
primary_culture = sarniryabsad
religion = high_philosophy
technology_group = tech_raheni
religious_school = starry_eye_school
capital = 4418

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Chandrasen"
		dynasty = "Harudesh"
		birth_date = 1408.9.27
		adm = 3
		dip = 2
		mil = 2
		culture = west_harimari
	}
}