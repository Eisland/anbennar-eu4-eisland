government = monarchy
add_government_reform = jamindar_reform
government_rank = 1
primary_culture = sarniryabsad
religion = high_philosophy
technology_group = tech_raheni
religious_school = silk_turban_school
capital = 4410

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Ganyakan"
		dynasty = "i Dhenijansar"
		birth_date = 1390.2.17
		adm = 5
		dip = 3
		mil = 3
		culture = west_harimari
	}
}