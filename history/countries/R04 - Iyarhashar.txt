government = monarchy
add_government_reform = plutocratic_reform
government_rank = 1
primary_culture = raghamidesh
religion = high_philosophy
technology_group = tech_raheni
religious_school = golden_palace_school
capital = 4372

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Nar Singh"
		dynasty = "i Iyarhashar"
		birth_date = 1389.11.1
		adm = 3
		dip = 5
		mil = 4
		culture = west_harimari
	}
}