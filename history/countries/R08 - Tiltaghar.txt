government = monarchy
add_government_reform = great_jamindar_reform
government_rank = 1
primary_culture = rajnadhid
add_accepted_culture = west_harimari
religion = high_philosophy
technology_group = tech_raheni
religious_school = unbroken_claw_school
capital = 4466

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }

1444.1.1 = {
	monarch = {
		name = "Sahtiya"
		dynasty = "i Tiltaghar"
		birth_date = 1415.1.5
		adm = 1
		dip = 1
		mil = 6
		culture = west_harimari
	}
}