government = monarchy
add_government_reform = feudalism_reform
government_rank = 1
primary_culture = high_lorentish
religion = regent_court
technology_group = tech_cannorian
capital = 158 # Lasean
national_focus = MIL

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }
