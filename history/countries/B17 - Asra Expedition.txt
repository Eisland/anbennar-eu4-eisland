government = theocracy
add_government_reform = adventurer_reform
government_rank = 1
primary_culture = silver_dwarf
religion = regent_court
technology_group = tech_dwarven
capital = 849

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }

1442.3.17 = {
	monarch = {
		name = "Jarmun"
		dynasty = "Asra"
		birth_date = 1290.11.4
		adm = 3
		dip = 4
		mil = 6
	}
	add_ruler_personality = immortal_personality
}